package com.example.fufs;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.fufs.DBHelper.DBHelper;
import com.example.fufs.DBHelper.PesquisasDBHelper;
import com.example.fufs.pesquisa.Pesquisa;
import com.example.fufs.usuario.Usuario;


/**
 * A simple {@link Fragment} subclass.
 */
public class MinhasPesquisasFragment extends Fragment {

//    ListView listMinhasPesquisas;
//    DBHelper db;
    public MinhasPesquisasFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_minhas_pesquisas, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        PesquisasDBHelper pesquisasDBHelper = new PesquisasDBHelper(getContext());
//        db = new DBHelper(getContext());
//
//        Usuario usuario = db.getUsuario(getActivity().getSharedPreferences("user_preferences", Context.MODE_PRIVATE).getString("email", "email"));
//
////        listMinhasPesquisas = getActivity().findViewById(R.id.listMinhasPesquisas);
//
//        TextView txtAlerta = getActivity().findViewById(R.id.txtAlert);
//
//        if (pesquisasDBHelper.listPesquisaFromUser(usuario.getId()) != null) {
//            txtAlerta.setVisibility(View.VISIBLE);
//            try {
//                ArrayAdapter<Pesquisa> ListArrayAdapter = new ArrayAdapter<Pesquisa>(getActivity(), android.R.layout.simple_list_item_1, pesquisasDBHelper.listPesquisaFromUser(usuario.getId()));
//                listMinhasPesquisas.setAdapter(ListArrayAdapter);
//            } catch (NoClassDefFoundError e){
//                Log.d("error: ", String.valueOf(e));
//            }
//        }

    }
}
