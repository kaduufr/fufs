package com.example.fufs;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class EditUserActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_user);

        getActionBar().setTitle("Editar: ");

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
