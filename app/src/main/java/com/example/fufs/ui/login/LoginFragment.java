package com.example.fufs.ui.login;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;


import com.example.fufs.CadastroFragment;
import com.example.fufs.DBHelper.DBHelper;
import com.example.fufs.HomeActivity;
import com.example.fufs.R;
import com.example.fufs.usuario.Usuario;




public class LoginFragment extends Fragment {

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    Button btnEntrar, btnGoCadastro;
    EditText editEmail, editSenha;

    DBHelper dbHelper;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.login_fragment, container, false);

        return view ;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        dbHelper = new DBHelper(getActivity());


        editEmail = (EditText) getActivity().findViewById(R.id.editEmail);
        editSenha = (EditText) getActivity().findViewById(R.id.editSenha);

        btnEntrar = (Button) getActivity().findViewById(R.id.btnEntrar);
        btnEntrar.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                Usuario usr = dbHelper.getUsuario(editEmail.getText().toString());
                    if ((usr.getEmail().equals(editEmail.getText().toString())) && (usr.getSenha().equals(editSenha.getText().toString()))){
                        Intent intent = new Intent(getActivity(), HomeActivity.class);
                        intent.putExtra("email", usr.getEmail());

                        SharedPreferences.Editor editor = getContext().getSharedPreferences("user_preferences", Context.MODE_PRIVATE).edit().putString("email", usr.getEmail());
                        editor.apply();

                        startActivity(intent);
                        getActivity().finish();
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                        builder.setTitle("Login ou senha Incorretos");
                        builder.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
            }
        });
        btnGoCadastro = (Button) getActivity().findViewById(R.id.btnGoCadastro);
        btnGoCadastro.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().beginTransaction()
                        .replace(R.id.container, new CadastroFragment())
                        .addToBackStack(null)
                        .commit();
            }
        });
    }
}
