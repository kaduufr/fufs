package com.example.fufs.pesquisa;

import java.io.Serializable;
import java.util.ArrayList;

public class Pesquisa implements Serializable {

    private int id;
    private String titulo;
    private String descricao;
    private ArrayList<String> perguntas;



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public ArrayList<String> getPerguntas() {
        return perguntas;
    }

    public void setPerguntas(String nomePergunta) {
        this.perguntas.add(nomePergunta);
    }

    //    @Override
//    public String toString() {
//        return "Titulo: " + getTitulo() + "\nDescrição: " + getDescricao();
//    }
    public String toString() {
        return getTitulo();
    }
}
