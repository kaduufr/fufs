package com.example.fufs;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class NovaPesquisaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nova_pesquisa);
    }

    @Override
    protected void onStart() {
        super.onStart();
        getSupportActionBar().setTitle("Nova Pesquisa");
    }
}
