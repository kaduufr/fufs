package com.example.fufs;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.fufs.DBHelper.PesquisasDBHelper;
import com.example.fufs.pesquisa.Pesquisa;

import java.util.ArrayList;
import java.util.List;

public class EditPesquisaActivity extends AppCompatActivity {

    String tituloPq;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_pesquisa);

        final ArrayList<String> perguntas = new ArrayList<String>();

        Pesquisa recuperarPesquisa = new Pesquisa();
        if (savedInstanceState == null){
            Bundle extra = getIntent().getExtras();
            if (extra != null){
                tituloPq = extra.getString("tituloPesquisa", "Sem Titulo");
            }
        }

        ListView listPerguntas = (ListView) findViewById(R.id.listPerguntas);

        TextView txtTituloDaPesquisa = findViewById(R.id.txtTituloDaPesquisa);
        txtTituloDaPesquisa.setText(tituloPq);

        FloatingActionButton fabNovaPergunta = findViewById(R.id.fabAddNovaPergunta);
        if (fabNovaPergunta != null) {
            fabNovaPergunta.setOnClickListener(new FloatingActionButton.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog alerta;

                    AlertDialog.Builder builder = new AlertDialog.Builder(EditPesquisaActivity.this);
                    builder.setTitle("Nova Pergunta");
                    final EditText input = new EditText(EditPesquisaActivity.this);
                    input.layout(8,16,8,16);
                    input.setInputType(InputType.TYPE_TEXT_VARIATION_LONG_MESSAGE);
                    input.setHint("Digite aqui...");
                    builder.setView(input);
                    builder.setPositiveButton("Criar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            String texto ="Pergunta: " + input.getText().toString();
                            perguntas.add(texto);
                            dialog.dismiss();

                        }
                    });
                    alerta = builder.create();
                    alerta.show();

                }
            });
        }

        ArrayAdapter<String> ListArrayAdapter = new ArrayAdapter<String>(EditPesquisaActivity.this, android.R.layout.simple_list_item_1, perguntas);
        listPerguntas.setAdapter(ListArrayAdapter);

        Button btnSalvarAlteracoes = (Button) findViewById(R.id.btnSalvarAlteracoes);
        btnSalvarAlteracoes.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                Toast.makeText(EditPesquisaActivity.this, "Dados Atualizados Com Sucesso", Toast.LENGTH_SHORT);
            }
        });

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        finish();
    }
}
