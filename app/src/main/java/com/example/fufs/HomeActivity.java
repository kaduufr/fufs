package com.example.fufs;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.fufs.usuario.Usuario;

import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity {

    private TextView mTextMessage;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    getSupportActionBar().setTitle("Feed Pesquisas");
                    getSupportFragmentManager().beginTransaction().replace(R.id.containerFragments, new ListaPesquisasFragment()).commit();
                    return true;
                case R.id.navigation_dashboard:
                    getSupportActionBar().setTitle("Minhas Pesquisas");
                    getSupportFragmentManager().beginTransaction().replace(R.id.containerFragments, new MinhasPesquisasFragment())
                            .addToBackStack(null).commit();
                    return true;
                case R.id.navigation_notifications:
                    getSupportActionBar().setTitle("Perfil");
                    getSupportFragmentManager().beginTransaction().replace(R.id.containerFragments, new PerfilFragment())
                            .addToBackStack(null).commit();
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        if (savedInstanceState == null){
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.containerFragments, new ListaPesquisasFragment())
                    .commitNow();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        final AlertDialog alerta;

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Deseja sair??");
        builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(HomeActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
        builder.setNegativeButton("Não", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alerta = builder.create();
        alerta.show();
    }
}
