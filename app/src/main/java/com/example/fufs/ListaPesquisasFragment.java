package com.example.fufs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.fufs.DBHelper.DBHelper;
import com.example.fufs.DBHelper.PesquisasDBHelper;
import com.example.fufs.pesquisa.Pesquisa;
import com.example.fufs.usuario.Usuario;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListaPesquisasFragment extends Fragment {

    ListView listaPesquisas;
    FloatingActionButton fabNovaPesquisa;
    PesquisasDBHelper pesquisasDBHelper;

    EditText editTituloPesquisa;
    EditText editDescricaoPesquisa;

    DBHelper dbHelper;

    public ListaPesquisasFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_lista_pesquisas, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        pesquisasDBHelper = new PesquisasDBHelper(getActivity());
        listaPesquisas = (ListView) getActivity().findViewById(R.id.listaPesquisas);
        listaPesquisas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView textView = (TextView) view;
                Intent intent = new Intent(getActivity(), EditPesquisaActivity.class);
                intent.putExtra("tituloPesquisa", textView.getText().toString());
                startActivity(intent);
            }
        });

        if (pesquisasDBHelper.listAllPesquisas() != null) {
            ArrayAdapter<Pesquisa> ListArrayAdapter = new ArrayAdapter<Pesquisa>(getActivity(), android.R.layout.simple_list_item_1, pesquisasDBHelper.listAllPesquisas());
            ListArrayAdapter.notifyDataSetChanged();
            listaPesquisas.setAdapter(ListArrayAdapter);
        }

        fabNovaPesquisa = getActivity().findViewById(R.id.fabNovaPesquisa);
        if (fabNovaPesquisa != null) {
            fabNovaPesquisa.setOnClickListener(new FloatingActionButton.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog alerta;

                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setTitle("Nova Pesquisa");
                    final EditText input = new EditText(getContext());
                    input.layout(8,8,8,0);
                    input.setInputType(InputType.TYPE_TEXT_VARIATION_LONG_MESSAGE);
                    input.setHint("Digite o Titulo");
                    builder.setView(input);
                    builder.setPositiveButton("Criar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            String texto = input.getText().toString();

                            pesquisasDBHelper.addPesquisa(texto, "");

                            Intent intent = new Intent(getActivity(),EditPesquisaActivity.class);
                            intent.putExtra("tituloPesquisa", texto);
                            startActivity(intent);
                        }
                    });
                    alerta = builder.create();
                    alerta.show();

                }
            });
        }

    }
}
