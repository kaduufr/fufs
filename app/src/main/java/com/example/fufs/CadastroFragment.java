package com.example.fufs;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.fufs.DBHelper.DBHelper;
import com.example.fufs.ui.login.LoginFragment;
import com.example.fufs.usuario.Usuario;

/**
 * A simple {@link Fragment} subclass.
 */
public class CadastroFragment extends Fragment {

    DBHelper dbHelper;
    Usuario user = new Usuario();
    Button btnCadastrar;
    Button btnVoltarToLogin;

    EditText lblNome, lblSenha, lblEmail, lblMatricula;

    public CadastroFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_cadastro, container, false);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();


        btnVoltarToLogin = (Button) getActivity().findViewById(R.id.btnVoltarToLogin);
        btnVoltarToLogin.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().beginTransaction()
                        .replace(R.id.container, new LoginFragment())
                        .commit();
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        lblEmail = (EditText) getActivity().findViewById(R.id.lblEmail);
        lblNome = (EditText) getActivity().findViewById(R.id.lblNome);
        lblSenha = (EditText) getActivity().findViewById(R.id.lblSenha);
        lblMatricula = (EditText) getActivity().findViewById(R.id.lblMatricula);

        dbHelper = new DBHelper(getActivity());

        user.setNome(lblNome.getText().toString());
        user.setSenha(lblNome.getText().toString());
        user.setEmail(lblNome.getText().toString());
        user.setMatricula(lblMatricula.getText().toString());

        btnCadastrar = (Button) getActivity().findViewById(R.id.btnCadastrar);
        btnCadastrar.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                dbHelper.addUsuario(lblNome.getText().toString(), lblEmail.getText().toString(), lblMatricula.getText().toString(), lblSenha.getText().toString());
                dbHelper.close();
                SharedPreferences.Editor editor = getContext().getSharedPreferences("user_preferences", Context.MODE_PRIVATE).edit().putString("email", lblEmail.getText().toString());
                editor.apply();
                Intent intent = new Intent(getActivity(), HomeActivity.class);
                startActivity(intent);
            }
        });
    }
}
