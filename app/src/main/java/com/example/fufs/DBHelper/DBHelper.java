package com.example.fufs.DBHelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.fufs.usuario.Usuario;

import java.util.ArrayList;


public class DBHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "usuarios";

    private static final String TABLE_USUARIOS = "usuarios";
    private static final String ID = "id";
    private static final String NOME = "nome";
    private static final String EMAIL = "email";
    private static final String MATRICULA = "matricula";
    private static final String SENHA = "senha";

    private static final String[] COLUNAS = {ID, NOME, EMAIL, MATRICULA, SENHA};

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE =  "CREATE TABLE usuarios (" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "nome TEXT," +
                "email TEXT," +
                "matricula TEXT," +
                "senha TEXT)";
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS usuarios");
        this.onCreate(db);
    }

    public void addUsuario(String nome, String email, String matricula, String senha) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(NOME, nome);
        values.put(EMAIL, email);
        values.put(MATRICULA, matricula);
        values.put(SENHA, senha);

        db.insert(TABLE_USUARIOS, null, values);
        db.close();
    }

    private Usuario cursorToUsuario(Cursor cursor) {
        Usuario user = new Usuario();
        user.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex("id"))));
        user.setNome(cursor.getString(cursor.getColumnIndex("nome")));
        user.setEmail(cursor.getString(cursor.getColumnIndex("email")));
        user.setMatricula(cursor.getString(cursor.getColumnIndex("matricula")));
        user.setSenha(cursor.getString(cursor.getColumnIndex("senha")));
        return user;
    }

    public Usuario getUsuario(String email) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_USUARIOS, COLUNAS, "email= ?", new String[] {String.valueOf(email)} , null, null, null, null);
        if (cursor == null) {
            return null;
        } else {
            cursor.moveToFirst();
            Usuario usuario = cursorToUsuario(cursor);
            return usuario;
        }
    }

    public ArrayList<Usuario> listAllUser(){
        ArrayList<Usuario> listaUsuarios = new ArrayList<Usuario>();

        String query = "SELECT * FROM " + TABLE_USUARIOS;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        while (cursor.moveToNext()){
            Usuario readUser = new Usuario();
            readUser.setId(Integer.parseInt(cursor.getString(0)));
            readUser.setNome(cursor.getString(1));
            readUser.setEmail(cursor.getString(2));
            readUser.setMatricula(cursor.getString(3));
            readUser.setSenha(cursor.getString(4));

            listaUsuarios.add(readUser);
        }
        return listaUsuarios;
    }

}
