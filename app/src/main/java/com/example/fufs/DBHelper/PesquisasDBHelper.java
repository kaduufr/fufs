package com.example.fufs.DBHelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.fufs.pesquisa.Pesquisa;

import java.util.ArrayList;

public class PesquisasDBHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 3;
    private static final String DATABASE_NAME = "pesquisas";

    private static final String table_pesquisas = "pesquisas";
    private static final String ID = "id";
    private static final String TITULO = "titulo";
    private static final String DESCRICAO = "descricao";
    private static final String UserID = "userID";

    private static final String[] colunas = {ID, TITULO, DESCRICAO, UserID};

    public PesquisasDBHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE pesquisas (" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "titulo TEXT," +
                "descricao TEXT)";
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS pesquisas");
        this.onCreate(db);
    }
//add campo userID devolta
    public void addPesquisa(String titulo, String descricao){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(TITULO, titulo);
        values.put(DESCRICAO, descricao);

        db.insert(table_pesquisas, null, values);
        db.close();
    }

    public ArrayList<Pesquisa> listAllPesquisas(){
        ArrayList<Pesquisa> listaPesquisas = new ArrayList<Pesquisa>();

        String query = "SELECT * FROM " + table_pesquisas;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        while (cursor.moveToNext()){
            Pesquisa pesquisa = new Pesquisa();
            pesquisa.setId(Integer.parseInt(cursor.getString(0)));
            pesquisa.setTitulo(cursor.getString(1));
            pesquisa.setDescricao(cursor.getString(2));

            listaPesquisas.add(pesquisa);
        }
        return listaPesquisas;
    }

    public ArrayList<Pesquisa> listPesquisaFromUser(int userID){
        ArrayList<Pesquisa> listaPesquisasDoUsuario = new ArrayList<Pesquisa>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(table_pesquisas, colunas, "userID = ?", new String[]{String.valueOf(userID)}, null, null, null, null);

        if (cursor == null){
            return null;
        } else {
            while (cursor.moveToNext()) {
                Pesquisa pqs = new Pesquisa();

                pqs.setId(Integer.parseInt(cursor.getString(0)));
                pqs.setTitulo(cursor.getString(1));
                pqs.setDescricao(cursor.getString(2));

                listaPesquisasDoUsuario.add(pqs);

            }
        }
        return listaPesquisasDoUsuario;
    }

    public Pesquisa getPesquisa(String tituloPesquisa){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(table_pesquisas, colunas, "titulo = ?", new String[]{String.valueOf(tituloPesquisa)}, null, null, null, null);
        if (cursor == null){
            return null;
        } else {
            cursor.moveToFirst();
            Pesquisa pesquisa = new Pesquisa();

            pesquisa.setId(Integer.parseInt(cursor.getString(0)));
            pesquisa.setTitulo(cursor.getString(1));
            pesquisa.setDescricao(cursor.getString(2));

            return pesquisa;
        }
    }
}
