package com.example.fufs;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.example.fufs.DBHelper.DBHelper;
import com.example.fufs.usuario.Usuario;


/**
 * A simple {@link Fragment} subclass.
 */
public class PerfilFragment extends Fragment {

    private EditText lblNomeUsuario, lblEmailUsuario, lblMatriculaUsuario;

    DBHelper dbHelper;

    public PerfilFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_perfil, container, false);

        lblNomeUsuario = (EditText) view.findViewById(R.id.lblNomeUsuario);
        lblMatriculaUsuario = (EditText) view.findViewById(R.id.lblMatriculaUsuario);
        lblEmailUsuario = (EditText) view.findViewById(R.id.lblEmailUsuario);

        SharedPreferences preferences = getContext().getSharedPreferences("user_preferences", Context.MODE_PRIVATE);

        dbHelper = new DBHelper(getContext());
        Usuario userOn = dbHelper.getUsuario(preferences.getString("email", "email invalido"));

        if (userOn != null){
            lblEmailUsuario.setText(userOn.getEmail());
            lblNomeUsuario.setText(userOn.getNome());
            lblMatriculaUsuario.setText(userOn.getMatricula());
        }
        if (preferences.contains("email")){
        } else {
            lblEmailUsuario.setText("Error");
            lblMatriculaUsuario.setText("Error");
            lblNomeUsuario.setText("Error");
        }

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        FloatingActionButton fabEdit = (FloatingActionButton) getActivity().findViewById(R.id.fabEdit);

        fabEdit.setOnClickListener(new FloatingActionButton.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "Serviço indisponivel!!", Toast.LENGTH_SHORT).show();
            }
        });

    }
}
