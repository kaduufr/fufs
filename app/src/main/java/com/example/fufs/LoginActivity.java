package com.example.fufs;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.fufs.ui.login.LoginFragment;

public class LoginActivity extends AppCompatActivity {

    private static final String Arquivo = "RegistUFS.txt";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, LoginFragment.newInstance())
                    .commitNow();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        finish();
    }
}
